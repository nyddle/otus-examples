package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/davecgh/go-spew/spew"
)

//go:generate jsonenums -type=ShirtSize

type ShirtSize byte

const (
	NA ShirtSize = iota
	XS
	S
	M
	L
	XL
)

//go:generate jsonenums -type=WeekDay

type WeekDay int

const (
	Monday WeekDay = iota
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	Sunday
)

func main() {
	v := struct {
		Size ShirtSize
		Day  WeekDay
	}{M, Friday}
	if err := json.NewEncoder(os.Stdout).Encode(v); err != nil {
		log.Fatal(err)
	}

	spew.Dump(v)

	input := `{"main.Size":"XL", "main.Day":"Dimarts"}`
	if err := json.NewDecoder(strings.NewReader(input)).Decode(&v); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("decoded %s as %+v\n", input, v)
}

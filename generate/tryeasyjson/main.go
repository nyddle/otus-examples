package main

import (
	"encoding/json"
	"generate/tryeasyjson/student"
)

func main() {
	s := student.Student{"alex", 31, []string{"go", "python"}}

	data, err := json.Marshal(&s)
	if err != nil {
		panic(err)
	}
	print(string(data))
}

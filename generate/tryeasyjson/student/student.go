package student

type Student struct {
	Name    string
	Age     int
	Courses []string
}

//go:generate ./command.sh

package main

import (
	"fmt"
)

func main() {
	fmt.Println("if you type 'go generate' in this directory command.sh will be run")
}
